import './Counter.css'
import { useCallback, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { increment, decrement } from '../store/SliceCounter'
import { login, logout } from '../store/SliceAuth'

export default function Counter() {


    const golbalState = useSelector(state => state);
    const dispatch = useDispatch();

    const isLogin = () => {
        return golbalState.auth.isAuth;
    }

    const handelAuth = status => {

        if (status) {
            dispatch(logout())
        } else {
            dispatch(login())
        }

    }

    const handelCounter = useCallback((type, value) => {
        if (type === 'increment') {
            dispatch(increment(value))
        } else {
            dispatch(decrement(value))
        }
    }, [dispatch])

    useEffect(() => {
        handelCounter('increment', 5);
    }, [handelCounter]);


    return (
        <div className="wrapper-counter">

            <button className="mb-2" onClick={() => handelAuth(isLogin())} >{isLogin() ? 'Logout' : 'Login'}</button>

            {isLogin() &&

                <>
                    <h1 className="counter">Counter : {golbalState.counter.value}</h1>
                    <button onClick={() => handelCounter('increment', 5)}>increment</button>
                    <button onClick={() => handelCounter('decrement', 5)}>decrement</button></>
            }


        </div>
    )
}
