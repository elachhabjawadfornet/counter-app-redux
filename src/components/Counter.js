import './Counter.css'
import { useSelector, useDispatch } from 'react-redux'
import { useCallback, useEffect } from 'react'

export default function Counter() {


    const state = useSelector(state => state);
    const dispatch = useDispatch();


    const handelCounter = state => {
        return state < 1 ? 'Counter is less than 1' : state;
    }


    const operationsCounter = useCallback((type, payload) => {


        dispatch({ type, payload });


    }, [dispatch]);

    const toogleCounter = (type) => {
        dispatch({ type });
    }


    useEffect(() => {
        operationsCounter('increment', 5)
    }, [operationsCounter])





    return (
        <div className="wrapper-counter">

            <button className="mb-2" onClick={() => toogleCounter('showCounter')}>Hide / show counter</button>

            {state.showCounter &&

                <>
                    <h1 className="counter">Counter : {handelCounter(state.value)}</h1>
                    <button onClick={() => operationsCounter('increment', 5)}>increment</button>
                    <button onClick={() => operationsCounter('decrement', 5)}>decrement</button>
                </>}


        </div>
    )
}
