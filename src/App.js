import './App.css';
import Counter from './components/TCounter'
import store from './store'
import { Provider } from 'react-redux'

function App() {
  return (
    <div className="container">
      <Provider store={store}>
        <Counter />
      </Provider>
    </div>
  );
}

export default App;
