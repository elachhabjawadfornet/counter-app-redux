import { configureStore } from '@reduxjs/toolkit'
import reducerCounter from './SliceCounter'
import reducerAuth from './SliceAuth'



export default configureStore({ reducer: { counter: reducerCounter, auth: reducerAuth } });

