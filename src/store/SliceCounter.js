import { createSlice } from '@reduxjs/toolkit'
import { logout } from './SliceAuth'


const initialState = { value: 0 };

const counterSlice = createSlice({
    name: 'counter',
    initialState,
    reducers: {
        increment: (state, action) => {
            state.value += action.payload
        },

        decrement: (state, action) => {
            if (!(state.value < 1)) {
                state.value -= action.payload
            }

        }
    },
    extraReducers: {
        [logout]: (state, action) => {
            state.value = 0;
        },
    }

});



export default counterSlice.reducer;

export const { increment, decrement } = counterSlice.actions;

